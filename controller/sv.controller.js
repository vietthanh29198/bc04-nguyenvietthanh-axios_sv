renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((sv) => {
    var contentTr = `<tr>
    <td>${sv.id}</td>
    <td>${sv.name}</td>
    <td>${sv.email}</td>
    <td>${sv.average()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.id
    }')" class="btn btn-danger">Xóa</button>
    <button onclick="suaSinhVien('${
      sv.id
    }')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`;
    contentHTML += contentTr;
  });
  console.log("contentHTML: ", contentHTML);
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function openLoading() {
  document.getElementById("loading").style.display = "flex";
}
function closeLoading() {
  document.getElementById("loading").style.display = "none";
}
