const BASE_URL = "https://62de511fccdf9f7ec2d50a65.mockapi.io";

function getDSSV() {
  openLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);

      var listSv = res.data.map(
        (sv) =>
          new SinhVienWithId(
            sv.id,
            sv.name,
            sv.email,
            sv.password,
            sv.math,
            sv.physics,
            sv.chemistry
          )
      );
      renderDSSV(listSv);
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}
getDSSV();

function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function resetData() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = " ";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

// Put: cập nhật
function suaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      document.getElementById("txtMaSV").value = res.data.id;
      document.getElementById("txtTenSV").value = res.data.name;
      document.getElementById("txtEmail").value = res.data.email;
      document.getElementById("txtPass").value = res.data.password;
      document.getElementById("txtDiemToan").value = res.data.math;
      document.getElementById("txtDiemLy").value = res.data.physics;
      document.getElementById("txtDiemHoa").value = res.data.chemistry;
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capNhat() {
  const maSv = document.getElementById("txtMaSV").value;
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  let sv = new SinhVienWithoutId(
    tenSv,
    email,
    matKhau,
    diemToan,
    diemLy,
    diemHoa
  );
  openLoading();
  axios({
    url: `${BASE_URL}/sv/${maSv}`,
    method: "PUT",
    data: sv,
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

// Post: thêm mới
function themSV() {
  const tenSv = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  let newSV = new SinhVienWithoutId(
    tenSv,
    email,
    matKhau,
    diemToan,
    diemLy,
    diemHoa
  );
  openLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}
